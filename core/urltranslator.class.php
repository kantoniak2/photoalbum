<?php

defined('STARTED') or die ('Restricted access.');

class URLTranslator {

	protected $baseAdress;
	protected $defaultModule;
	protected $defaultAction;
	
	public function __construct($baseAdress) {
	
		$this->baseAdress = $baseAdress;
		// TODO singleton
	
	}
	
	public function setDefaults($module, $action) {
		$this->defaultModule = $module;
		$this->defaultAction = $action;
	}
	
	public function seoAdress($input) {
	
		$input = trim($input);
		
		$input = preg_replace('^(index\.php)?\?', '', $input);
		$input = explode('&', $input);
		
		foreach($input as $i => $element) {
			$element = explode('=', $element);
			$output[$element[0]] = $element[1];
		}
	
		if (!$output['module'] || !$output['action'])
			$return = $this->baseAdress .DS. $this->defaultModule .DS. $this->defaultAction .DS;
		else
			$return = $this->baseAdress .DS. $output['module'] .DS. $output['action'] .DS;
	
		foreach($output as $name => $value) {
			if ($name != 'module' && $name != 'action') {
				if (count($output) == 1 && $name = 'null')
					$return .= $value .DS;
				else if (count($output) == 3 && isset($output['module']) && isset($output['action']) && $name == 'null')
					$return .= $value .DS;
				else
					$return .= $name .DS. $value .DS;
			}
		}
	
		$return = preg_replace('//', '/', $return);
		$return = preg_replace(':/', '://', $return);
		return $return;
	
	}

}

?>