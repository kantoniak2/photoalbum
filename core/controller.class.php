<?php

class Controller {

	public $moduleName;
	public $actionName;
	protected $module;
	
	public function __construct($options = array()) {
	
		// TODO: TRANSLACJA .htaccess i obróbka adresu
		
		if (!$_GET['module'] || !$_GET['action']) { // Nie wiedziałem czy zrobić łączenie czy zastępowanie, może URL Translator będzie zastępował / jakimś defaultem?
			$_GET = array_merge(Config::$site['def_get'], $_GET);
		}
		
		$this->moduleName = $_GET['module'];
		$this->actionName = $_GET['action'];
	
	}

	public function dispatch() {
	
		try {
		
			if (!include_once BASEPATH.'modules/'. $this->moduleName .'/module.class.php')
				throw new RuntimeException('Could not load module file.');
				
			if (!class_exists('Module') || !method_exists('Module', $this->actionName))
				throw new RuntimeException('Invalid class.');
			
			$this->module = new Module($this);
			
			$actionName = $this->actionName;
			$this->module->$actionName();
			$output = $this->module->display();
			echo $output;
		
		} catch (Exception $e) {
		
			//prelog($e->getMessage());
			include_once BASEPATH.'modules/error/errorpage.class.php';
			$this->module = new ErrorPage($this);
			$this->module->show404();
			$output = $this->module->display();
			echo $output;
		
		}
	
		// Debugjerski
		exit();
	
	}
}

?>