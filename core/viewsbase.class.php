<?php

class ViewsBase {

	protected $params = array();
	
	public function set($name, $value) {
        $this->params[$name] = $value;
    }

	public function fetch($templateName) {
	
        ob_start();
		
		if (!include_once(BASEPATH.'templates/'. $templateName .'.php'))
			throw new RuntimeException('Could not load template file from: '.BASEPATH.'templates/'. $templateName .'.php');
        
		$tmp = ob_get_contents();
        ob_end_clean();
        return $tmp;
        
    }
	
}

?>