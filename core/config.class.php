<?php

defined("STARTED") or die("<p>Unauthorized access.</p>");

class Config {

	public static $site = Array(
		"title" => "PhotoAlbum",
		"baseurl" => "http://localhost/photoalbum/",
		"def_get" => array('module' => 'photos', 'action' => 'all')
	);
	
	public static $db = Array(
		"host" => "localhost",
		"user" => "root",
		"password" => "",
		"dbname" => "photoalbum",
		"prefix" => "phl_"
	);

	public static $debug = Array(
		"error_reporting" => 1
	);

}

define('DBPREFIX', Config::$db['prefix']);

?>
