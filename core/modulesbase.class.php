<?php

abstract class ModulesBase {

	protected $controller;
	protected $view;
	protected $template;

	public function __construct(& $controller) {
	
		$this->controller = $controller;
		
		if (!include_once(BASEPATH.'modules/'. $controller->moduleName .'/view.class.php')) {
			if (!include_once(BASEPATH.'core/viewsbase.class.php'))
				throw new RuntimeException('Could not load view file.');
		}
			
		if (!class_exists('View')) {
			if (!class_exists('ViewsBase'))
				throw new RuntimeException('Invalid class.');
			else
				$this->view = new ViewsBase();
		} else
			$this->view = new View();		
	
	}
	
	public function display() {
		
		if (!$this->template)
			throw new RuntimeException('No template.');
	
        $this->set('module', $this->controller->moduleName);
        $this->set('action', $this->controller->actionName);
        
        return $this->view->fetch($this->template);
	}
    
	
	protected function set($name, $value) {
		$this->view->set($name, $value);
	}
	
}

?>