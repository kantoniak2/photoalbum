<?php

class DBManager {
	private static $dbinstance;
	private static $db;

	public function __construct() {
		self::$db = new mysqli(Config::$db['host'],Config::$db['user'],Config::$db['password'],Config::$db['dbname']);
		if (mysqli_connect_errno()) {
			throw new RuntimeException('Błąd MySQL: '.mysqli_connect_error());
		}
		self::$db->query("SET NAMES utf8");
	}

	public static function instance() {
		if (!self::$dbinstance) {
			self::$dbinstance = new DBManager(); 
		}

		return self::$db;
	}
}

// Czy to może tutaj zostać?
function db_fetchall($rs) {
	$result = Array();
	while($row = $rs->fetch_assoc())
		$result[] = $row;
	return $result;
}

?>