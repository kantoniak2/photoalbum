<?php

function getCounter() {

	// Read number of users
	$file = fopen('counter.txt', 'r');
	$number = fgets($file);
	fclose($file);
	$number++;
	
	// Save new number
	$file = fopen('counter.txt', 'w');
	fwrite($file, $number);

	return $number;
	
}

?>