<?php

class Poll {

	protected $file;
	protected $path;
	
	function __construct($path) {
	
		$this->path = $path;
		@ $fileHandle = fopen($path, 'r');
		
		if (!$fileHandle)
			throw new Exception('Nie wczytano pliku.');
		
		while ($this->file[] = fgets($fileHandle)) {
			if ($i) {
				$this->file[$i] = preg_replace('/\s+$/', '', $this->file[$i]);
				$this->file[$i] = explode('|', $this->file[$i]);
			} else {
				$this->file = preg_replace('/\s+$/', '', $this->file);
			}
			$i++;
		}
		
		fclose($fileHandle);
	}

	function getTitle() {
		return $this->file[0];
	}
	
	function getAnswers() {
		
		foreach ($this->file as $line) {
			if ($i && $line[0] != '') {
				$answers[] = $line[0];
			}
			$i++;
		}
		return $answers;
	}
	
	function getScore() {
	
		foreach ($this->file as $line) {
			if ($i && $line[0] != '') {
				$answers[] = $line;
			}
			$i++;
		}
		return $answers;	
	}
	
	function vote($answer) {
	
		$this->file[$answer][1]++;
	
	}
	
	function votesSum() {
	
		foreach($this->file as $answer) {
			$return += $answer[1];
		}
	
		return $return;
	
	}
	
	function highest() {
	
		foreach($this->file as $key => $answer) {
			if ($key > 0) {
				if ($answer[1] > $return)
					$return = $answer[1];
			}
		}
	
		return $return;
	
	}
	
	function createFile() {
	
		$return = $this->file[0];
		
		foreach ($this->file as $line) {
			if ($i>0 && $line[1] != '') {
				$return .= "\n".$line[0].'|'.$line[1];
			}
			$i++;
		}
		
		$file = fopen($this->path, 'w');
		fwrite($file, $return);
		
		return $return;
	
	}
	
	function dump() {
		echo '<pre>';
		print_r($this->file);
		echo '</pre>';
	}

}

?>