<?php

require_once INCPATH.'top.php';

if ($this->params['errorMessage']):
	echo '<p class="dialog d_error">'. $this->params['errorMessage'] .'</p>';
elseif ($this->params['infoErrorMessage']):
	echo '<header><h2>Ostatnie wpisy</h2></header>';
	echo '<p class="dialog d_info">'. $this->params['infoErrorMessage'] .'</p>';
else:
	
	$pager = $this->params['pager'];
	
	if ($this->params['pageTitle']):
		echo '<header><h2>'. $this->params['pageTitle'] .'</h2>';
	else:
		echo '<header><h2>Ostatnie wpisy</h2>';
	endif;
	
	if ($pager) {
	
	echo '<div class="header_submenu">';
			
		if ($pager->hasPrevious()) { echo '<a href="?page='. $pager->getPrevious() .'">&laquo;</a>'; }
		echo '<p>Strona '. $pager->getCurrentPage() .' z '. $pager->getPages() .'</p>';
		if ($pager->hasNext()) { echo '<a href="?page='. $pager->getNext() .'">&raquo;</a>'; }
			
	echo '</div>';
	
	}
	
	echo '</header><div id="main_inner">';
	
	if ($_GET['info'])
		echo '<p class="dialog d_info">'. htmlspecialchars($_GET['info']) .'</p>';
	
foreach ($this->params['photos'] as $photo) {

$photo['desc'] = str_replace("\r\n\r\n", '</p><p>', $photo['desc']);
$photo['desc'] = str_replace("\r\n", '<br />', $photo['desc']);

?>
	
	<div class="photo_entry">
			
				<p class="dest"><?=$photo['location']?></p>
				
				<img src="<?=$photo['image_url']?>" alt="<?=$photo['location']?>" />
				
				<?php if ($photo['image_license']): ?>
				<p class="license">
					<?=$photo['image_license']?>
					<?php if ($photo['image_authorlink']) echo '<a href="'. $photo['image_authorlink'] .'">'; ?>
					<?=$photo['image_author']?>
					<?php if ($photo['image_authorlink']) { ?></a><?php } ?>
				</p>
				<?php endif; ?>
				
				<div class="desc">
				
					<h3><?=$photo['title']?></h3>
					<p class="info">
						<span class="author"><?=$photo['author']?></span>
						<span class="time"><?=$photo['datetime']?></span>
					</p>
					
					<p><?=$photo['desc']?></p>
				
				</div>
			
			</div>
	
<?php
}
	
	if ($pager) {
	
		echo '<div id="bottom_pager">';
			if ($pager->hasPrevious()) { echo '<a href="?page='. $pager->getPrevious() .'" class="fleft"><span class="icon">&laquo;</span><span class="text">Poprzednia strona</span></a>'; }
			if ($pager->hasNext()) { echo '<a href="?page='. $pager->getNext() .'" class="fright"><span class="text">Następna strona</span><span class="icon">&raquo;</span></a>'; }
		echo '</div>';
	
	}
	
	echo '</div>';

endif;

require_once INCPATH.'rightcol.php';
require_once INCPATH.'footer.php';

?>