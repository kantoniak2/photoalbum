	</div>
	
	<div id="rightcol">
	
		<?php if ($_SESSION['id']): ?>
		<div>
		
			<h3>Administruj</h3>
			
			<ul>
				<li><a href="<?=Config::$site['baseurl']?>?module=photos&action=search">Wpisy</a></li>
				<li><a href="<?=Config::$site['baseurl']?>?module=photos&action=admin_add">Dodaj wpis</a></li>
				<li><a href="<?=Config::$site['baseurl']?>?module=user&action=logout">Wyloguj się</a></li>
			</ul>
		
		</div>
		<?php endif; ?>
		
		<?php
		
			function currentURL() {
			
				$pageURL = 'http://';
				
				if ($_SERVER["SERVER_PORT"] != "80")
					$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
				else
					$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
					
				return $pageURL;
			}
		
			try {
			
				if (!include_once LIBSPATH.'poll.class.php')
					throw new Exception();
				
				$poll = new Poll('poll.txt');
				$show_poll = '';
				
				if ($_SESSION['poll'] || !$_SESSION['poll'] && isset($_POST['sent'])) {
	
					if (isset($_POST['sent'])) {
	
						// Add new score
						$answer = $_POST['answer'];
						$answer++;
	
						$poll->vote($answer);
						$poll->createFile();
						$_SESSION['poll'] = true;
	
					}
	
					$votes = $poll->votesSum();
					$highest = $poll->highest();
	
					$show_poll .= '<h4>'. $poll->getTitle() .'</h4>';
					foreach($poll->getScore() as $id=>$answer) {
					
						$percent = $answer[1] / $votes;
						$show_poll .= '<p>'. $answer[0] .': '. round($percent * 100) .'%</p><div style="width: '. round(10 + ($answer[1] / $highest) * 170) .'px;">&nbsp;</div>';
					
					}
	
				} else {
	
					// Show poll
					$show_poll .= '<form action="'. currentURL() .'" method="post">';
					$show_poll .= '<h4>'. $poll->getTitle() .'</h4>';
	
					foreach($poll->getAnswers() as $id=>$answer) {
				
						$show_poll .= '<p><input type="radio" name="answer" value="'. $id .'" class="clear checkbox" /><span>'. $answer .'</span></p>';
	
					}
	
					$show_poll .= '<input id="poll_submit" type="submit" name="sent" value="Głosuj" />';
					$show_poll .= '</form>';
	
				}
			
			} catch (Exception $e) { $show_poll = false; }
		
			if ($show_poll):
		
		?>
		
		<div id="module_poll">
		
			<h3>Ankieta</h3>
			
			<?=$show_poll?>
		
		</div>
		
		<?php endif; ?>
		
		<div>
		
			<h3>Inne projekty</h3>
			<ul>
				<li><a href="http://krzant.w.staszic.waw.pl/sinus">Szatański sinus</a></li>
				<li><a href="http://krzant.w.staszic.waw.pl/kartki">Serwis kartkowy</a></li>
				<li><a href="http://amleather.pl/temp/terminal/">Emulator konsoli, ONP</a></li>
			</ul>
			
		</div>
		
		<?php
		
			try {
			
				if (!include_once LIBSPATH.'counter.php')
					throw new Exception();
					
				$counter = getCounter();
				
				echo '<div style="padding-bottom: 0">';
				echo '<h3 style="margin-bottom: 0">'. $counter .' odwiedzin</h3>';
				echo '</div>';
			
			} catch (Exception $e) {}
		
		?>
	
	</div>