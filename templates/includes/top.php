<?php defined('STARTED') or die('Restricted access'); ?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8" />
	<title>PhotoAlbum</title>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script> -->
	<script src="script/jquery.js" type="text/javascript"></script>
<style>

/* Reset */
* { margin: 0; padding: 0; border: 0; }
body { text-align: center; background: #3F61A6; }

/* Pasek nagłówka */
#topbar { width: 100%; height: 60px; background: #2E2E2E; }
#header { width: 1000px; height: 60px; margin: 0 auto; padding: 0 27px 0 33px; background: #393939; }
h1 a { float: left; width: 288px; height: 18px; margin: 21px 0; background: url(images/logo.png); font-size: 0; }
#header ul { float: right; height: 28px; margin: 16px 0; }
#header li, #header li a { float: left; list-style-type: none; height: 28px; line-height: 28px; }
#header li a { margin-left: 6px; padding: 0 8px; font-family: 'PT Sans', 'Trebuchet MS'; font-size: 12px; color: #FFF; text-decoration: none; }
#header li a:hover { background: #3F61A6; }

/* Pomocnicze */
#main { overflow: hidden; width: 1060px; margin: 15px auto 0 auto; text-align: left; }
.fleft { float: left; }
.fright { float: right; }
.lspace { margin-left: 15px; }
.clear { clear: both; }
.clearnone { clear: none; }
.width500 { width: 500px !important; }
.bmargin5 { margin-bottom: 5px !important; }
.tmargin10 { margin-top: 10px !important; }

/* Lewa kolumna + nagłówek */
#leftcol { width: 810px; float: left; }
#leftcol>header { float: left; width: 760px; height: 60px; margin-bottom: 15px; padding: 0 20px 0 30px; background: #2E2E2E; }
#leftcol>header>h2 { float: left; font-family: 'PT Sans', 'Trebuchet MS'; font-size: 21px; font-weight: bold; line-height: 62px; color: #C44C31; text-transform: uppercase; }
#leftcol .header_submenu { float: right; }
#leftcol .header_submenu>* { float: left; margin-top: 15px; height: 30px; font: 12px 'Tahoma', 'Arial'; line-height: 28px; color: #FFF; text-align: center; text-decoration: none; }
#leftcol .header_submenu a { width: 30px; font-size: 14px; background: #C44C31; }
#leftcol .header_submenu a:hover { background: #8A3522; }
#leftcol .header_submenu p { padding: 0 10px; background: #383838; }

/* Dolna paginacja */
#bottom_pager { float: left; width: 100%; margin: -15px 0 15px 0; }
#bottom_pager a { height: 30px; font: 12px 'Tahoma', 'Arial'; line-height: 28px; color: #FFF; background: #E35839; text-align: center; text-decoration: none; }
#bottom_pager a .icon { display: inline-block; width: 30px; height: 30px; font-size: 14px; background: #C44C31; }
#bottom_pager a:hover .icon { background: #8A3522; }
#bottom_pager a .text { display: inline-block; padding: 0 10px; }

/* Prawa kolumna */
#rightcol { width: 220px; float: right; }
#rightcol>div { overflow: hidden; background: #4367B0; margin-bottom: 20px; padding: 0 20px 14px 20px; }
#rightcol h3 { width: 205px; height: 36px; margin: 0 0 14px -20px; padding-left: 15px; font: 16px 'PT Sans', 'Trebuchet MS'; font-weight: bold; line-height: 34px; text-transform: uppercase; color: #EEE; background: #273C66; }
#rightcol p, #rightcol a { font: 12px 'Tahoma', 'Arial'; text-align: left; line-height: 20px; text-decoration: none; color: #111; }
#rightcol a { color: #CCC; }
#rightcol a:hover { color: #999; }
#rightcol ul { margin-left: 15px; list-style-type: square; color: #333; line-height: 23px; }

#module_poll h4 { color: #222; margin-bottom: 10px; font: 14px 'Tahoma', 'Arial'; font-weight: bold; }
#module_poll div { margin: 4px 0 10px 0; height: 3px; background: #273C66; }
#module_poll form span { float: left; height: 16px; margin-left: 2px; }
#module_poll input { margin-top: 3px; margin-bottom: 6px; }
#module_poll #poll_submit { width: 180px; height: 26px; margin: 0; margin-top: 10px; cursor: pointer; }

/* Stopka */
#footer { float: left; width: 100%; background: #2E2E2E; }
#footer_inner { width: 1020px; height: 70px; margin: 0 auto; padding: 0 20px; background: #393939; }
#footer p { float: right; margin-top: 17px; font: 13px 'Tahoma', 'Arial'; line-height: 18px; text-align: right; color: #999; }
#footer a { color: #666; text-decoration: none; }

/* Komunikaty */
.dialog { background: #222; float: left; width: 740px; margin-bottom: 15px; padding: 20px 20px 20px 50px; font: 12px 'Tahoma', 'Arial'; color: #666; line-height: 160%; }
.dialog a { color: #999; text-decoration: none; }
.dialog a:hover { color: #666; }
.d_info { color: #CCC; background: #273C66 url(images/d_info.png) 20px 22px no-repeat; }
.d_error { color: #CCC; background: #66272D url(images/d_error.png) 20px 22px no-repeat; }
.d_warn { color: #333; background: #b79A18 url(images/d_warn.png) 20px 22px no-repeat; }

/* Formularze */
form { float: left; margin-bottom: 5px; font: 12px 'Tahoma', 'Arial'; line-height: 32px; color: #CCC; }
form label, form input, form select, form textarea { float: left; margin-bottom: 10px; background: #2F497D; }
form.longlabels label { width: 180px; }
form label { clear: both; width: 100px; padding: 0 10px; background: #273C66; color: #CCC; }
form input { width: 386px; height: 32px; padding: 0 7px; color: #CCC; }
form textarea { width: 372px; height: 200px; padding: 12px 14px; font: 12px 'Tahoma', 'Arial'; color: #CCC; line-height: 160%; }
form select { width: 395px; height: 32px; padding-left: 5px; color: #CCC; }
form .short { width: 195px; }
form .checkbox { float: left; height: 14px; width: 14px; padding: 9px 8px 9px 9px; background: #2F497D; }
form .checkbox input { width: 14px; height: 14px; }
form .button { width: 160px; cursor: pointer; }
form .button.long { width: 300px; }
form .button:hover { background: #273C66; }
form .submit { clear: both; width: 200px; height: 40px; }

/* Tabele */
table { width: 810px; margin-bottom: 15px; padding: 15px 20px 20px; font: 12px 'Tahoma', 'Arial'; color: #CCC; background: #2E2E2E; border-spacing: 0; }
thead td { font-weight: bold; border-bottom: 1px solid #444; }
tbody tr:nth-child(even) td { background: #333; }
td { height: 34px; padding-left: 10px; }
td.id { width: 25px; text-align: center; }
td.center { padding: 0 10px; text-align: center; }
td a { color: #5B8CEF; text-decoration: none; }
td a:hover { color: #91B5FF; }

/* Wpisy */
#leftcol_inner { margin-top: 15px; }
.photo_entry { float: left; background: #2E2E2E; margin-bottom: 30px; }
.dest { float: left; width: 750px; height: 50px; padding: 0 30px; font-family: 'PT Sans', 'Trebuchet MS'; font-size: 14px; font-weight: bold; line-height: 50px; text-transform: uppercase; color: #EEE; }
.photo_entry img { float: left; width: 810px; }
.license { float: right; clear: both; height: 36px; margin-top: -36px; padding: 0 15px; opacity: 0.3; font: 12px 'Tahoma', 'Arial'; line-height: 36px; background: #FFF; }
.license a { color: #000; text-decoration: none; }
.license a:hover { font-weight: bold; }
.desc { float: left; padding: 36px 40px 25px 40px; }
.desc h3 { float: left; height: 32px; margin-bottom: 12px; font-family: 'PT Sans', 'Trebuchet MS'; font-size: 24px; font-weight: bold; line-height: 32px; color: #C44C31; }
.desc p.info { float: right;  width: auto !important; color: #FFF; line-height: 25px; margin: 2px 0; }
.desc p.info span { display: inline-block; height: 26px; padding: 0 7px 0 6px; }
.desc p.info .author { background: #666; }
.desc p.info .time { background: #C44C31; }
.desc p { float: left; width: 730px; margin-bottom: 15px; font-family: Tahoma; font-size: 12px; text-align: justify; line-height: 150%; color: #AAA; }

/* Photo form */
#license_data { margin-left: 30px; clear: both; }
#license_data input { width: 356px; }

</style>

<script>
$(document).ready(function () {

	resizeMain();
	$(window)
		.scroll(resizeMain)
		.resize(resizeMain);

});

function resizeMain() {

	console.log($(window).height(), $('html').height());

	$('#main').css('min-height', 0);
	
	if ($(window).height() > $('html').height())
		$('#main').css('min-height', $('#main').height() + $(window).height() - $('html').height());

}
</script>

</head>
<body>

<div id="topbar">
	<div id="header">
	
		<h1><a href="<?=Config::$site['baseurl']?>">PhotoAlbum - Ciekawe miejsca w fotografii</a></h1>
		
		<ul id="topmenu">
		
			<li><a href="<?=Config::$site['baseurl']?>">Wszystkie wpisy</a></li>
			<li><a href="<?=Config::$site['baseurl']?>?action=search">Szukaj</a></li>
			
			<?php if (!$_SESSION['id']): ?>
				<li><a href="<?=Config::$site['baseurl']?>?module=user&amp;action=login">Logowanie</a></li>
			<?php else: ?>
				<li><a href="<?=Config::$site['baseurl']?>?module=user&amp;action=logout">Wyloguj</a></li>
			<?php endif; ?>
		
		</ul>
	
	</div>
</div>

<div id="main">

	<div id="leftcol">