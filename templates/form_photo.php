<?php
require_once INCPATH.'top.php';
?>

<header><h2><?=$this->params['formTitle']?></h2></header>

<?php
if ($this->params['errorMessage']):
	echo '<p class="dialog d_error">'. $this->params['errorMessage'] .'</p>';
elseif ($this->params['infoErrorMessage']):
	echo '<p class="dialog d_info">'. $this->params['infoErrorMessage'] .'</p>';
endif;

$photo = $this->params['photoData'];

?>

<form action="<?=$this->params['formLocation']?>" class="longlabels" method="post">

	<label>Tytuł:</label>
	<input type="text" name="photo[title]" value="<?=$photo['title']?>" />
	
	<label>Lokacja:</label>
	<input type="text" name="photo[location]" value="<?=$photo['location']?>" />
	
	<label>URL obrazka:</label>
	<input type="url" name="photo[image_url]" value="<?=$photo['image_url']?>" />
	
	<div class="checkbox clear"><input type="checkbox" name="use_license" id="use_license" <?php if ($photo['image_license']): ?> checked="checked"<?php endif; ?> /></div>
	<label class="clearnone width500">Obrazek jest udostępniany w ramach licencji</label>
	
	<div id="license_data">
	
		<label>Rodzaj licencji:</label>
		<input type="text" name="photo[image_license]" value="<?=$photo['image_license']?>" />
		
		<label>Autor obrazka:</label>
		<input type="text" name="photo[image_author]" value="<?=$photo['image_author']?>" />
	
		<label>URL autora (opcjonalnie):</label>
		<input type="url" name="photo[image_authorlink]" value="<?=$photo['image_authorlink']?>" />
	
	</div>
	
	<label>Treść wpisu:</label>
	<textarea name="photo[desc]"><?=$photo['desc']?></textarea>
	
	<input type="submit" name="sent" class="button submit long tmargin10" value="Wyślij" />

</form>

<script>
$(document).ready(function () {

	if ($('#use_license:checked').length == 0) {
		$('#license_data').hide();
	}
	
	$('#use_license').change(function () {
	
		if ($('#use_license:checked').length == 1) {
			$('#license_data').show('fast');
		} else {
			$('#license_data').hide('fast');
		}
	
	});

});
</script>

<?php
require_once INCPATH.'rightcol.php';
require_once INCPATH.'footer.php';
?>