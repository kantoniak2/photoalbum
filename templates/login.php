<?php
require_once INCPATH.'top.php';
?>

<header><h2>Logowanie</h2></header>
<p class="dialog d_warn">Dane dostępu do konta testowego można uzyskać od właściciela strony.</p>	

<?php
if ($this->params['errorMessage']):
	echo '<p class="dialog d_error">'. $this->params['errorMessage'] .'</p>';
elseif ($this->params['infoErrorMessage']):
	echo '<p class="dialog d_info">'. $this->params['infoErrorMessage'] .'</p>';
endif;
?>

<form action="?module=user&action=login" method="post">

	<label>Użytkownik:</label>
	<input type="text" name="user[login]" value="<?=$_POST['user']['login']?>" />
	
	<label>Hasło:</label>
	<input type="password" name="user[password]" value="<?=$_POST['user']['password']?>" />
	
	<input type="submit" name="sent" class="button submit long tmargin10" value="Zaloguj się" />

</form>

<?php
require_once INCPATH.'rightcol.php';
require_once INCPATH.'footer.php';
?>