<?php

require_once INCPATH.'top.php';

if ($this->params['errorMessage']):
	echo '<p class="dialog d_error">'. $this->params['errorMessage'] .'</p>';
elseif ($this->params['infoErrorMessage']):
	echo '<header><h2>Wyszukiwarka</h2></header>';
	echo '<p class="dialog d_info">'. $this->params['infoErrorMessage'] .'</p>';
else:

	$photos = $this->params['photos'];

	?><header><h2>Lista wpisów</h2></header>
	
	<?php if ($_GET['info'])
		echo '<p class="dialog d_info">'. htmlspecialchars($_GET['info']) .'</p>'; ?>
		
	<?php if ($_GET['error'])
		echo '<p class="dialog d_error">'. htmlspecialchars($_GET['error']) .'</p>'; ?>
	
	<form>
	
		<label>Szukaj:</label>
		<input type="text" id="searcher" />
		
		<select class="lspace short" id="searcher_criteria">
			<option value="1" selected="selected">Miejsce</option>
			<option value="2">Tytuł</option>
			<option value="3">Data</option>
			<option value="4">Użytkownik</option>
		</select>
	
	</form>
	
	<div id="results">
	<!-- JavaScript results display -->
	</div>

<script type="text/javascript">
var elements = [
<?php

foreach($photos as $i => $photo) {

	echo "\t[";
	echo "'". ++$id ."', '". $photo['location']."', '". $photo['title']."', '". $photo['datetime']."', '". $photo['author']."', '". $photo['id']."'";
	echo ']';
	if ($i+1 != count($photos))
		echo ',';
	echo "\n";

}

?>
];

function showTable(pattern) {

	if (!pattern) {
		showTableHTML(elements);
	} else {
	
		var ret = [];
		
		for(var i in elements) {
			if (elements[i][$('#searcher_criteria').val()].toLowerCase().search(new RegExp(pattern.toLowerCase())) != -1) {
				ret[ret.length] = elements[i];
			}
		}
		
		showTableHTML(ret);
	}
	
}

function showTableHTML(table) {

	var tableData = '<table><thead><tr><td class="id">#</td><td>Miejsce</td><td>Tytuł artykułu</td><td>Data wpisu</td><td class="center">Użytkownik</td>';
	<?php if ($_SESSION['id']): ?>
	tableData += '<td class="center">Edytuj</td><td class="center">Usuń</td>';
	<?php endif; ?>
	tableData += '</tr></thead><tbody>';
	
	for(var i in table) {
	
		tableData += '<tr>';
		
		tableData += '' +
		'<td class="id">'+ table[i][0] +'</td>'+
		'<td>'+ table[i][1] +'</td>'+
		'<td><a href="?module=photos&action=show&id='+ table[i][5] +'">'+ table[i][2] +'</a></td>'+
		'<td>'+ table[i][3] +'</td>'+
		'<td class="center">'+ table[i][4] +'</td>'+
		<?php if ($_SESSION['id']): ?>
		'<td class="center width70"><a href="?module=photos&action=admin_edit&id='+ table[i][5] +'">Edytuj</a></td>'+
		'<td class="center width70"><a href="?module=photos&action=admin_delete&id='+ table[i][5] +'">Usuń</a></td>'+
		<?php endif; ?>
		'</tr>';
	
	}
	
	tableData += '</tbody></table>';
	
	if (table.length == 0)
		tableData = '<p class="dialog d_long d_info">Brak pasujących wpisów</p>';
	
	$('#results').html(tableData);
	
}

$(document).ready(function () {

	showTable();
	
	$('#searcher').keyup(function () {
		console.log( Number($('#searcher_criteria').val()) );
		console.log($('#searcher_criteria').val());
		showTable($('#searcher').val());
	});

});

</script>

<?php

endif;

require_once INCPATH.'rightcol.php';
require_once INCPATH.'footer.php';

?>