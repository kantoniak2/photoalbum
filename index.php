<?php

session_start();

function prelog($data) {
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}

class InfoException extends Exception {}

define('STARTED', 1);
define('BASEPATH', str_replace("\\",'/',dirname(__FILE__)).'/');
define('DS', '/');
define('INCPATH', BASEPATH.DS.'templates'.DS.'includes'.DS);
define('LIBSPATH', BASEPATH.DS.'libs'.DS);

// Do debugera z tym
ini_set('display_errors',1); 
error_reporting(E_ALL ^ E_WARNING ^ E_NOTICE);

// Ładowanie potrzebnych bibliotek
require_once 'core/controller.class.php';
require_once 'core/config.class.php';
require_once 'core/modulesbase.class.php';
require_once 'core/viewsbase.class.php';
require_once 'core/dbmanager.class.php';

$db = DBManager::instance();

// Wykonanie żądania
$controller = new Controller();
$controller->dispatch();

?>