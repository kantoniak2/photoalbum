<?php

class Module extends ModulesBase {

	protected $photosByPage = 2;

	public function admin_add() {
	
		if (!$_SESSION['id']) {
			header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Dział wymaga uprawnień administratora!');
			return;
		}
	
		try {
		
			if (!$_POST['sent'])
				throw new Exception('');
			
			$this->set('photoData', $_POST['photo']);
			
			foreach ($_POST['photo'] as $key => $element) {
				if ($element === '' && $key != 'image_license' && $key != 'image_author' && $key != 'image_authorlink')
					throw new InfoException('Nie wypełniono wszystkich pól formularza.');
			}
			
			$data = $this->createDataArray($_POST['photo']);
			
			$query = 'INSERT INTO '. DBPREFIX .'photos SET';
			foreach ($data as $key => $value)
				$query .= " `".$key."`='". addslashes($value) ."',";
				
			$query = str_replace("'NOW()',", 'NOW()', $query);
			$query = str_replace("'NULL'", 'NULL', $query);
			
			$db = DBManager::instance();
			$result = $db->query($query);
			
			if ($result == 1)
				header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&info=Dodano wpis');
			else
				throw new Exception('Nie udało się dodać wpisu.');
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
	
		$this->set('formTitle', 'Dodaj wpis');
		$this->set('formLocation', '?module=photos&action=admin_add');
		$this->template = 'form_photo';
	
	}
	
	public function admin_edit() {
	
		if (!$_SESSION['id']) {
			header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Dział wymaga uprawnień administratora!');
			return;
		}
	
		if (!$_GET['id'] || !is_numeric($_GET['id'])) {
			header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Nie podano identyfikatora wpisu do edycji!');
			return;
		}
		
		try {
		
			$db = DBManager::instance();
		
			if (!$_POST['sent']) {
			
				$photoData = $db->query('SELECT * FROM '.DBPREFIX."photos WHERE id='". $_GET['id'] ."'");
				
				if (!$photoData || !$photoData->num_rows) {
					header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Nie udało się załadować danych zdjęcia do edycji!');
					return;
				} else {
					$photoData = db_fetchall($photoData);
					$this->set('photoData', $photoData[0]);
				}
			
				throw new Exception('');	
			}
			
			$this->set('photoData', $_POST['photo']);
			
			foreach ($_POST['photo'] as $key => $element) {
				if ($element === '' && $key != 'image_license' && $key != 'image_author' && $key != 'image_authorlink')
					throw new InfoException('Nie wypełniono wszystkich pól formularza.');
			}
			
			$data = $this->createDataArray($_POST['photo'], false);
			
			$query = 'UPDATE '. DBPREFIX .'photos SET';
			foreach ($data as $key => $value)
				$query .= " `".$key."`='". addslashes($value) ."',";
			$query = substr($query, 0, -1);
			$query .= " WHERE id='". $_GET['id'] ."'";
			$query = str_replace("'NULL'", 'NULL', $query);
			
			$result = $db->query($query);
			
			if ($result == 1)
				header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&info=Zedytowano wpis');
			else
				throw new Exception('Nie udało się zapisać zmian wpisu.');
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
	
		$this->set('formTitle', 'Edytuj wpis');
		$this->set('formLocation', '?module=photos&action=admin_edit&id='. $_GET['id']);
		$this->template = 'form_photo';
	
	}
	
	public function admin_delete() {
	
		if (!$_SESSION['id']) {
			header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Dział wymaga uprawnień administratora!');
			return;
		}
	
		if (!$_GET['id'] || !is_numeric($_GET['id'])) {
			header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Nie podano identyfikatora wpisu do usunięcia!');
			return;
		}
		
		try {
		
			$db = DBManager::instance();
			$result = $db->query('DELETE FROM '.DBPREFIX.'photos WHERE id='.$_GET['id']);
			
			if ($result == 1)
				header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&info=Usunięto wpis');
			else
				header('Location: '. Config::$site['baseurl'] .'?module=photos&action=search&error=Nie udało się usunąć wpisu');
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
		
	}
	
	public function show() {
		
		try {
		
			if (!is_numeric($_GET['id']) || !$_GET['id'])
				throw new InfoException('Nie podano prawidłowego identyfikatora wpisu.');
		
			$photo_data = $this->loadPhoto($_GET['id']);
		
			if (count($photo_data) == 1) {
				$this->set('pageTitle', 'Wpis #'.$_GET['id']);
			}
		
			$this->set(
				'photos',
				$photo_data
			);
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
	
		$this->template = 'main';
	
	}
	
	public function all() {
		
		if (!isSet($_GET['page']))
			$page = 1;
		else
			$page = $_GET['page'];
	
		try {
		
			if (!is_numeric($page))
				throw new InfoException('Podany numer strony jest nieprawidłowy.');
		
			$page--;
			$pagesNumber = $this->getPagesNumber();
			
			if ($pagesNumber === FALSE)
				throw new Exception('Nie udało się załadować danych.');
			else if (!$pagesNumber)
				throw new InfoException('Brak zdjęć do wyświetlenia.');
			
			$this->set(
				'photos',
				$this->loadPhotos(
					$this->photosByPage * $page,
					$this->photosByPage
				)
			);
			
			if (!include_once LIBSPATH.'pager.class.php')
				throw new Exception('Nie udało się załadować potrzebnych plików.');
			
			$pager = new Pager($pagesNumber, ++$page);
			$this->set('pager', $pager);
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
	
		$this->template = 'main';
	
	}
	
	
	public function search() {
	
		try {
		
			$this->set('photos', $this->loadPhotos());
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			$this->set('errorMessage', $e->getMessage());
		}
	
		$this->template = 'search';
		
	}
	
	protected function loadPhoto($id) {
	
		$db = DBManager::instance();
		
		$query = sprintf(
			'SELECT '. DBPREFIX .'photos.*, DATE_FORMAT('. DBPREFIX .'photos.datetime,\'%s\') AS datetime, '. DBPREFIX .'users.login AS author '.
			'FROM '. DBPREFIX .'photos '.
			'LEFT JOIN '. DBPREFIX .'users '.
			'ON '. DBPREFIX .'photos.author_id='. DBPREFIX .'users.id '.
			'WHERE '. DBPREFIX .'photos.id=%s',
				
			'%d.%m.%Y, %H:%i',
			$id
		);
		
		$photos = $db->query($query);
		
		if (!$photos)
			throw new Exception('Nie udało się załadować danych.');
		elseif (!$photos->num_rows)
			throw new InfoException('Chyba nie ma takiego zdjęcia ;)');
			
		return db_fetchall($photos);
	
	}
	
	protected function loadPhotos($start = false, $limit = false, $asc = false) {
	
		$db = DBManager::instance();
		$sort = $asc ? 'ASC' : 'DESC';
		$start = $start !== FALSE ? sprintf('LIMIT %d', $start) : '';
		$limit = $limit && $start ? sprintf(', %d', $limit) : '';
		
		$query = sprintf(
			'SELECT '. DBPREFIX .'photos.*, DATE_FORMAT('. DBPREFIX .'photos.datetime,\'%s\') AS datetime, '. DBPREFIX .'users.login AS author '.
			'FROM '. DBPREFIX .'photos '.
			'LEFT JOIN '. DBPREFIX .'users '.
			'ON '. DBPREFIX .'photos.author_id='. DBPREFIX .'users.id '.
			'ORDER BY datetime %s %s%s',
				
			'%d.%m.%Y, %H:%i',
			$sort,
			$start,
			$limit
		);
			
		$photos = $db->query($query);
		
		if (!$photos)
			throw new Exception('Nie udało się załadować danych.');
		elseif (!$photos->num_rows)
			throw new InfoException('W bazie nie ma aż tylu zdjęć ;)');
			
		return db_fetchall($photos);
	
	}
	
	protected function getPagesNumber() {
	
		$db = DBManager::instance();
		$rs = $db->query('SELECT id FROM '. DBPREFIX .'photos');
		
		if (!$rs)
			return false;
		else
			return ceil($rs->num_rows / $this->photosByPage);
	
	}
	
	protected function createDataArray($array, $insert=true) {
	
		if ($insert) {
			$array['author_id'] = $_SESSION['id'];
			$array['datetime'] = $array['datetime'] ? $array['datetime'] : 'NOW()';
		}
		
		$array['image_license'] = $array['image_license'] ? $array['image_license'] : 'NULL';
		$array['image_author'] = $array['image_author'] ? $array['image_author'] : 'NULL';
		$array['image_authorlink'] = $array['image_authorlink'] ? $array['image_authorlink'] : 'NULL';
		
		return $array;
	
	}

}

?>