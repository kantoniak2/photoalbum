<?php

class Module extends ModulesBase {

	public function login() {
	
		try {
		
			if (!$_POST['sent'])
				throw new Exception('');
			
			foreach ($_POST['user'] as $element) {
				if ($element === '')
					throw new InfoException('Nie wypełniono wszystkich pól formularza.');
			}
			
			$login = $_POST['user']['login'];
			$login = addslashes($login);
			
			$password = sha1($_POST['user']['password']);
			
			$db = DBManager::instance();
			$query = sprintf("SELECT id FROM ". DBPREFIX ."users WHERE login='%s' AND password='%s' LIMIT 1", $login, $password);
			$result = $db->query($query);
			
			if (!$result)
				throw new Exception('Nieprawidłowe zapytanie.');
			else if (!$result->num_rows)
				throw new Exception('Podano nieprawidłowe dane.');
				
			$result = db_fetchall($result);
			$_SESSION['id'] = $result[0]['id'];
			
			header('Location: '. Config::$site['baseurl'] .'?info=Zalogowano prawidłowo');
			
		} catch (InfoException $e) {
			$this->set('infoErrorMessage', $e->getMessage());
		} catch (Exception $e) {
			if ($e->getMessage())
				$this->set('errorMessage', $e->getMessage());
		}
	
		$this->template = 'login';
	
	}
	
	public function logout() {
	
		unset($_SESSION['id']);
		session_destroy();
		header('Location: '. Config::$site['baseurl'] .'?info=Wylogowano prawidłowo');
	
	}

}

?>